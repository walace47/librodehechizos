package com.joan.librohechizos.modelo;


import java.util.ArrayList;

public class HechizoAprendido extends Hechizo {

    public HechizoAprendido(String idHechizo,
                            String nombre,
                            String descripcion,
                            String aMayorNivel,
                            int rango,
                            int componenteVerbal,
                            int componenteSomatico,
                            int componenteMaterial,
                            String descripcionDelComponenteMaterial,
                            int ritual,
                            int concentracion,
                            String tiempoDeCasteo,
                            Escuela escuela,
                            int nivel,
                            String duracion,
                            ArrayList<Clase> clases) {

        super(idHechizo, nombre, descripcion, aMayorNivel, rango, componenteVerbal, componenteSomatico, componenteMaterial, descripcionDelComponenteMaterial, ritual, concentracion, tiempoDeCasteo, escuela, nivel, duracion, clases);
    }

    public HechizoAprendido(String nombre, String descripcion, String aMayorNivel, int rango, int componenteVerbal, int componenteSomatico, int componenteMaterial, String descripcionDelComponenteMaterial, int ritual, int concentracion, String tiempoDeCasteo, Escuela escuela, int nivel, String duracion, ArrayList<Clase> clases) {
        super(nombre, descripcion, aMayorNivel, rango, componenteVerbal, componenteSomatico, componenteMaterial, descripcionDelComponenteMaterial, ritual, concentracion, tiempoDeCasteo, escuela, nivel, duracion, clases);
    }

    public HechizoAprendido(
            String idHechizo,
            String nombre,
            int ritual,
            Escuela escuela,
            int nivel) {
        super(idHechizo, nombre, ritual, escuela, nivel);
    }

    public HechizoAprendido(String nombre, int nivel) {
        super(nombre, nivel);
    }

    public static HechizoAprendido toHechizoAprendido(Hechizo hechizo){
        return new HechizoAprendido(
                hechizo.getIdHechizo(),
                hechizo.getNombre(),
                hechizo.getRitual(),
                hechizo.getEscuela(),
                hechizo.getNivel()
        );
    }
}
