package com.joan.librohechizos.utiles;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;

import com.joan.librohechizos.R;
import com.joan.librohechizos.modelo.Personaje;
import com.joan.librohechizos.ui.LibroDeHechizos;
import com.joan.librohechizos.ui.ListaDotes;

public class Funcionalidad {
    public static boolean crearMenuLateral(MenuItem item, Context context, Personaje personaje){
        if (context.getClass().equals(LibroDeHechizos.class.getClass())){
            switch (item.getItemId()){
                case(R.id.menu_dotes):
                    Intent intent = new Intent(context, ListaDotes.class);
                    intent.putExtra("idPersonaje",personaje.getIdPersonaje());
                    context.startActivity(intent);
                    break;
                default:
                    break;
            }
        }

        return true;
    }
}
